#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <yas/board.hh>
#include <yas/board_item.hh>

#include <string>

TEST_CASE("Board::CreateFromSerialization() from invalid serialization",
	"[board]")
{
	using yas::Board;

	// Empty serialization.
	const std::string kInvalidBoardStr0 = "";
	auto board0 = Board::CreateFromSerialization(kInvalidBoardStr0);
	REQUIRE(board0 == nullptr);

	// Invalid characters in the serialization.
	const std::string kInvalidBoardStr1 = "hello world";
	auto board1 = Board::CreateFromSerialization(kInvalidBoardStr1);
	REQUIRE(board1 == nullptr);

	// Invalid board content, no players.
	const std::string kInvalidBoardStr2 =
		"#####\n"
		"###\n"
		"#####\n";
	auto board2 = Board::CreateFromSerialization(kInvalidBoardStr2);
	REQUIRE(board2 == nullptr);

	// Invalid board content, too many players.
	const std::string kInvalidBoardStr3 =
		"##@@#\n"
		"###\n"
		"#####\n";
	auto board3 = Board::CreateFromSerialization(kInvalidBoardStr3);
	REQUIRE(board3 == nullptr);
}


TEST_CASE("Board::CreateFromSerialization() from valid serialization",
	"[board]")
{
	using yas::Board;

	const std::string kValidBoard0 =
		"##########\n"
		"#  @ . #\n"
		"##########\n";
	auto board0 = Board::CreateFromSerialization(kValidBoard0);
	REQUIRE(board0 != nullptr);
}

TEST_CASE("Board::getItemAt() out of bound", "[board]")
{
	using yas::Board;

	const std::string kValidBoard0 =
		"##########\n"
		"#  @ . #\n"
		"##########\n";
	auto board0 = Board::CreateFromSerialization(kValidBoard0);
	REQUIRE(board0 != nullptr);
	REQUIRE(board0->getItemAt({100, 100}) == std::nullopt);
}

TEST_CASE("Board::getItemAt() within bound", "[board]")
{
	using yas::Board;
	using yas::BoardItem;

	const std::string kValidBoard0 =
		"##########\n"
		"#  @ . #\n"
		"##########\n";
	auto board0 = Board::CreateFromSerialization(kValidBoard0);
	REQUIRE(board0 != nullptr);
	REQUIRE(board0->getItemAt({0, 0}).value() == BoardItem::Wall);
	REQUIRE(board0->getItemAt({3, 1}).value() == BoardItem::Player);
	REQUIRE(board0->getItemAt({5, 1}).value() == BoardItem::Goal);
}

TEST_CASE("Board::movePlayer() when player cannot move", "[board]")
{
}

TEST_CASE("Board::movePlayer() when player can move, no box", "[board]")
{
}

TEST_CASE("Board::movePlayer() when player can move, boxes", "[board]")
{
}

TEST_CASE("BoardItemIsPlayer()", "[board_item]")
{
	using yas::BoardItem;

	REQUIRE(!yas::BoardItemIsPlayer(BoardItem::Box));
	REQUIRE(!yas::BoardItemIsPlayer(BoardItem::BoxOnGoal));
	REQUIRE(!yas::BoardItemIsPlayer(BoardItem::Floor));
	REQUIRE(!yas::BoardItemIsPlayer(BoardItem::Goal));
	REQUIRE(!yas::BoardItemIsPlayer(BoardItem::Wall));

	REQUIRE(yas::BoardItemIsPlayer(BoardItem::Player));
	REQUIRE(yas::BoardItemIsPlayer(BoardItem::PlayerOnGoal));
}

TEST_CASE("BoardItemIsBox()", "[board_item]")
{
	using yas::BoardItem;

	REQUIRE(!yas::BoardItemIsBox(BoardItem::Floor));
	REQUIRE(!yas::BoardItemIsBox(BoardItem::Goal));
	REQUIRE(!yas::BoardItemIsBox(BoardItem::Player));
	REQUIRE(!yas::BoardItemIsBox(BoardItem::PlayerOnGoal));
	REQUIRE(!yas::BoardItemIsBox(BoardItem::Wall));

	REQUIRE(yas::BoardItemIsBox(BoardItem::Box));
	REQUIRE(yas::BoardItemIsBox(BoardItem::BoxOnGoal));
}

