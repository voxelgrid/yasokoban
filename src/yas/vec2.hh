#pragma once

namespace yas {

template <typename T>
struct vec2 {
	union { T x; T w; };
	union { T y; T h; };

	vec2()
		: x(0), y(0)
	{}

	vec2(T x, T y)
		: x(x), y(y)
	{}
};

} // namespace yas

