#pragma once

namespace yas {

/// Represents an item on a Board.
enum class BoardItem {
	Box,
	BoxOnGoal,
	Floor,
	Goal,
	Player,
	PlayerOnGoal,
	Wall,
};

/// Returns |true| if a BoardItem can be considered as a player.
inline bool BoardItemIsPlayer(BoardItem item) {
	return (item == BoardItem::Player) || (item == BoardItem::PlayerOnGoal);
}

/// Returns |true| iff a BoardItem item can be considered as a box.
inline bool BoardItemIsBox(BoardItem item) {
	return (item == BoardItem::Box) || (item == BoardItem::BoxOnGoal);
}

} // namespace yas

