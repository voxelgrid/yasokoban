#include "board.hh"
#include "board_library.hh"
#include "log.hh"

#include <fstream>

using yas::Board;
using yas::BoardLibrary;

std::unique_ptr<BoardLibrary>
BoardLibrary::CreateFromFile(std::string const& path)
{
	auto library = std::make_unique<BoardLibrary>();

	std::ifstream file(path);
	if (!file) {
		ERR("error, could not open: %s\n", path.c_str());
		return nullptr;
	}

	size_t invalidBoardCount = 0;
	std::string boardBuf;
	std::string line;
	while (std::getline(file, line, '\n')) {
		if ((line.empty() || line[0] == ';') && !boardBuf.empty()) {
			auto board = Board::CreateFromSerialization(
				boardBuf);
			if (!board) {
				invalidBoardCount++;
			} else {
				library->mEntries.push_back(std::move(board));
			}
			boardBuf.clear();
		} else {
			boardBuf += line + '\n';
		}
	}

	return library;
}

size_t
BoardLibrary::count() const
{
	return mEntries.size();
}

std::unique_ptr<Board>
BoardLibrary::get(size_t num) const
{
	if (num >= count())
		return nullptr;
	return std::make_unique<Board>( *mEntries[num] );
}

