#include "board.hh"
#include "direction.hh"
#include "log.hh"

#include <optional>
#include <sstream>

using yas::Board;
using yas::BoardItem;
using yas::Direction;

static constexpr char kCharBox          = '$';
static constexpr char kCharBoxOnGoal    = '*';
static constexpr char kCharFloor        = ' ';
static constexpr char kCharGoal         = '.';
static constexpr char kCharPlayer       = '@';
static constexpr char kCharPlayerOnGoal = '+';
static constexpr char kCharWall         = '#';

static std::optional<BoardItem>
_BoardItemFromChar(char ch)
{
	switch (ch) {
	case kCharBox:
		return BoardItem::Box;
	case kCharBoxOnGoal:
		return BoardItem::BoxOnGoal;
	case kCharFloor:
		return BoardItem::Floor;
	case kCharGoal:
		return BoardItem::Goal;
	case kCharPlayer:
		return BoardItem::Player;
	case kCharPlayerOnGoal:
		return BoardItem::PlayerOnGoal;
	case kCharWall:
		return BoardItem::Wall;
	}

	return std::nullopt;
}

static bool
_DirectionToDxDy(Direction d, int *dx, int *dy)
{
	assert(dx);
	assert(dy);

	switch (d) {
	case Direction::Left:
		*dx = -1;
		*dy = 0;
		return true;
	case Direction::Right:
		*dx = 1;
		*dy = 0;
		return true;
	case Direction::Up:
		*dx = 0;
		*dy = -1;
		return true;
	case Direction::Down:
		*dx = 0;
		*dy = 1;
		return true;
	}

	return false;
}

std::unique_ptr<Board>
Board::CreateFromSerialization(std::string const& data)
{
	auto board = std::make_unique<Board>();

	// First pass, determine the dimension of the board.
	std::istringstream is(data);
	std::string buf;
	vec2<size_t> boardSize(0, 0);
	while (std::getline(is, buf, '\n')) {
		boardSize.w = std::max(boardSize.w, buf.length());
		boardSize.h += 1;
	}

	// Initialize the board private members to their initial state.
	board->mBoardSize = boardSize;
	board->mItems.assign(boardSize.w * boardSize.h, BoardItem::Wall);

	// Second pass, populate the board items for each characters
	// stored in the serialization.
	is = std::istringstream(data);
	vec2<size_t> itemPos(0, 0);
	bool playerFound = false;
	for (size_t i = 0; i < data.length(); ++i) {
		// End of line, skip to the next line.
		if (data[i] == '\n' || data[i] == EOF) {
			itemPos.x = 0;
			itemPos.y += 1;
			continue;
		}

		// Determine the right board item.
		auto item = _BoardItemFromChar(data[i]);
		if (!item) {
			ERR("invalid character: %c\n", data[i]);
			return nullptr;
		}

		board->mItems[itemPos.y * boardSize.w + itemPos.x] = *item;

		// If the item is a player, cache its position.
		if (BoardItemIsPlayer(*item)) {
			if (!playerFound) {
				playerFound = true;
				board->mPlayerPos = itemPos;
			} else {
				ERR("player position already found\n");
				return nullptr;
			}
		}

		itemPos.x += 1;
	}

	// Error if there was no player found.
	if (!playerFound) {
		ERR("could not find any player on the board\n");
		return nullptr;
	}

	return board;
}

std::optional<BoardItem>
Board::getItemAt(vec2<size_t> pos) const
{
	if ((pos.x >= mBoardSize.w) || (pos.y >= mBoardSize.h))
		return std::nullopt;
	return mItems[pos.y * mBoardSize.w + pos.x];
}

yas::vec2<size_t>
Board::getPlayerPosition() const
{
	return mPlayerPos;
}

yas::vec2<size_t>
Board::getSize() const
{
	return mBoardSize;
}

bool
Board::isCompleted() const
{
	// The board is completed iff all goals contain a box.
	for (size_t i = 0; i < mItems.size(); ++i) {
		bool incomplete = mItems[i] == BoardItem::Goal
			|| mItems[i] == BoardItem::PlayerOnGoal;
		if (incomplete)
			return false;
	}

	return true;
}

bool
Board::movePlayer(Direction d)
{

	int dx = 0;
	int dy = 0;
	if (!_DirectionToDxDy(d, &dx, &dy))
		return false;
	if (mPlayerPos.x == 0 && dx < 0)
		return false;
	if (mPlayerPos.y == 0 && dy < 0)
		return false;

	const auto currPos = mPlayerPos;
	const auto nextPos = vec2<size_t>(currPos.x + dx, currPos.y + dy);

	// The current item must be a player or an invariant has
	// been broken.
	auto currItem = getItemAt(currPos);
	assert(currItem);
	assert(BoardItemIsPlayer(*currItem));

	// Get the item next to the player.
	auto nextItem = getItemAt(nextPos);
	if (!nextItem)
		return false;

	std::optional<BoardItem> postMoveNextItem;
	switch (*nextItem) {
	case BoardItem::Box:
		if (_moveBox(nextPos, dx, dy))
			postMoveNextItem = BoardItem::Player;
		break;
	case BoardItem::BoxOnGoal:
		if (_moveBox(nextPos, dx, dy))
			postMoveNextItem = BoardItem::PlayerOnGoal;
		break;
	case BoardItem::Floor:
		postMoveNextItem = BoardItem::Player;
		break;
	case BoardItem::Goal:
		postMoveNextItem = BoardItem::PlayerOnGoal;
		break;
	default:
		break;
	}

	if (!postMoveNextItem)
		return false;

	// Update the type of the current item, previously a player.
	bool res = _setItemAt(currPos,
		*currItem == BoardItem::Player ? BoardItem::Floor : BoardItem::Goal);
	assert(res);

	// Update the position of the player and the next item.
	mPlayerPos = nextPos;
	res = _setItemAt(mPlayerPos, *postMoveNextItem);
	assert(res);

	// The move was successful, enqueue a barrier to inform
	// this is a reference state.
	mUndoMoveOps.push({ .type = UndoOp::Type::Barrier });

	return false;
}

bool
Board::undoLastPlayerMove()
{
	// Any processing error in this method is non-recoverable
	// and suggests an algorithmic error. Asserts if anything
	// can not be recovered.

	// If the undo stack is empty, nothing to be done.
	if (mUndoMoveOps.empty())
		return false;

	// If not empty, the undo stack must contain a barrier
	// entry at the top. If not, this means a move operation
	// was not completed correctly.
	assert(mUndoMoveOps.top().type == UndoOp::Type::Barrier);
	mUndoMoveOps.pop();

	while (!mUndoMoveOps.empty()) {
		const auto op = mUndoMoveOps.top();
		if (op.type == UndoOp::Type::Barrier)
			break;

		bool res = _setItemAt(op.pos, op.item, true);
		assert(res);

		// If this operation sets a player, update its
		// cached position.
		if (BoardItemIsPlayer(op.item))
			mPlayerPos = op.pos;

		// Remove the items.
		mUndoMoveOps.pop();
	}

	// This is a costly operation but we need to ensure we only
	// have one player on the board. This is an invariant that
	// must be kept.
	size_t numPlayers = 0;
	for (size_t i = 0; i < mItems.size(); ++i) {
		if (BoardItemIsPlayer(mItems[i])) {
			numPlayers += 1;
			assert(mPlayerPos.x == (i % mBoardSize.w));
			assert(mPlayerPos.y == (i / mBoardSize.w));
		}
		assert(numPlayers <= 1);
	}

	return true;
}

bool
Board::_moveBox(vec2<size_t> pos, int dx, int dy)
{
	if (pos.x == 0 && dx < 0)
		return false;
	if (pos.y == 0 && dy < 0)
		return false;

	// Get the current item, which must be a box.
	auto currItem = getItemAt(pos);
	assert(currItem);
	assert(BoardItemIsBox(*currItem));

	// Get the item next to the box.
	const auto nextPos = vec2<size_t>(pos.x + dx, pos.y + dy);
	auto nextItem = getItemAt(nextPos);
	if (!nextItem)
		return false;

	// Based on the item next to the box, determine whether the box can be
	// moved. Also, determine the type of the item next to the box post
	// move.
	std::optional<BoardItem> postMoveNextItem;
	switch (*nextItem) {
	case BoardItem::Floor:
		postMoveNextItem = BoardItem::Box;
		break;
	case BoardItem::Goal:
		postMoveNextItem = BoardItem::BoxOnGoal;
		break;
	default:
		break;
	}

	if (!postMoveNextItem)
		return false;
	assert(*postMoveNextItem == BoardItem::Box
		|| *postMoveNextItem == BoardItem::BoxOnGoal);

	// Update the type of the current item (previously a box).
	bool res = _setItemAt(pos,
		currItem == BoardItem::Box ? BoardItem::Floor : BoardItem::Goal);
	assert(res);

	// Update the type of the next item (now a box).
	res = _setItemAt(nextPos, *postMoveNextItem);
	assert(res);

	return true;
}

bool
Board::_setItemAt(vec2<size_t> pos, BoardItem item, bool isUndoing)
{
	if ((pos.x >= mBoardSize.w) || (pos.y >= mBoardSize.h))
		return false;

	// If we are not in the process of undoing operations, push
	// an operation that recover the state we are about to change.
	if (!isUndoing) {
		mUndoMoveOps.push(UndoOp {
			.type = UndoOp::Type::SetItem,
			.pos  = pos,
			.item = mItems[pos.y * mBoardSize.w + pos.x],
		});
	}

	mItems[pos.y * mBoardSize.w + pos.x] = item;

	return true;
}

