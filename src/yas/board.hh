#pragma once

#include <yas/board_item.hh>
#include <yas/direction.hh>
#include <yas/vec2.hh>

#include <memory>
#include <optional>
#include <stack>
#include <string>
#include <vector>

namespace yas {

/// Represents a Sokoban board.
class Board {
public:
	/// Creates a Board from a serialized copy.
	static std::unique_ptr<Board> CreateFromSerialization(
		std::string const& data);

	/// Returns the item stored at a specific location.
	std::optional<BoardItem> getItemAt(vec2<size_t> pos) const;

	/// Returns the position of the player.
	vec2<size_t> getPlayerPosition() const;

	/// Returns the size of the board.
	vec2<size_t> getSize() const;

	/// Returns |true| iff the board is completed.
	bool isCompleted() const;

	/// Moves the player into a direction.
	bool movePlayer(Direction dir);

	/// Undo the last player move, if possible.
	bool undoLastPlayerMove();

private:
	// Moves a box located at |pos|, in the |dx|, |dy| direction.
	bool _moveBox(vec2<size_t> pos, int dx, int dy);

	// Set an item at a position.
	bool _setItemAt(vec2<size_t> pos, BoardItem item,
		bool isUndoing = false);

private:
	vec2<size_t> mBoardSize;
	vec2<size_t> mPlayerPos;

	// Items present on the board.
	std::vector<BoardItem> mItems;

	// Represents an undo operation.
	struct UndoOp {
		enum class Type { Barrier, SetItem };
		Type type;
		vec2<size_t> pos;
		BoardItem item;
	};

	// Stack of operations that can be executed to undo
	// recent movements.
	std::stack<UndoOp> mUndoMoveOps;
};

} // namespace yas

