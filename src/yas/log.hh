#pragma once

#include <cstdio>

#define DBG(fmt, ...)                                      \
	do {                                               \
		fprintf(stdout, "[D] %s: " fmt,            \
			__func__, ##__VA_ARGS__);          \
	} while (0)


#define ERR(fmt, ...)                                      \
	do {                                               \
		fprintf(stderr, "[E] %s: " fmt,            \
			__func__, ##__VA_ARGS__);          \
	} while (0)
