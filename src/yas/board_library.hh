#pragma once

#include <memory>
#include <vector>

namespace yas {

/// Represents a library of Board.
class BoardLibrary {
public:
	/// Create a library from a file on disk.
	static std::unique_ptr<BoardLibrary> CreateFromFile(
		std::string const& path);

	/// Returns the number of boards in the library.
	size_t count() const;

	/// Returns the num-th element of the library.
	std::unique_ptr<Board> get(size_t num) const;

private:
	std::vector<std::unique_ptr<Board>> mEntries;
};

} // namespace yas
