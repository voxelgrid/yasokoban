#pragma once

namespace yas {

/// Represents a direction items can move toward.
enum class Direction { Down, Left, Right, Up };

} // namespace yas

