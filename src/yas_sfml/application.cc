#include "application.hh"

// Maximal supported board size.
static const size_t kMaxBoardW = 20;
static const size_t kMaxBoardH = 20;

// Calculate the size of a rendered tile based on its asset size.
static constexpr size_t kTileAssetW  = 128;
static constexpr size_t kTileSpriteS = 2;
static constexpr size_t kTileSpriteW = kTileAssetW / kTileSpriteS;

// Calculate the optimal window size.
static constexpr size_t kWinW = kTileSpriteW * kMaxBoardW;
static constexpr size_t kWinH = kTileSpriteW * kMaxBoardH;

std::unique_ptr<Application>
Application::Create(std::string const& assetDir)
{
	std::unique_ptr<Application> app(new Application);

	if (!app->_loadAssets(assetDir)) {
		fprintf(stderr, "could not load assets from directory: %s\n",
			assetDir.c_str());
		return nullptr;
	}

	assert(app->mBoardLibrary);

	if (app->mBoardLibrary->count() == 0) {
		fprintf(stderr, "could not find any boards\n");
		return nullptr;
	}

	// Select the first board.
	app->mCurrBoardInd = 0;
	app->mCurrBoard = app->mBoardLibrary->get(app->mCurrBoardInd);

	return app;
}

Application::Application()
	: mIsRunning(false)
	, mWindow(sf::VideoMode(kWinW, kWinH), ".: YAS :.")
	, mCurrBoardInd(0)
	, mCurrBoard(nullptr)
	, mBoardLibrary(nullptr)
{
	mWindow.setKeyRepeatEnabled(false);
	mWindow.setFramerateLimit(30);
}

void
Application::run()
{
	mIsRunning = true;

	while (mIsRunning) {
		sf::Event event;
		while (mWindow.pollEvent(event))
			_processEvent(event);
		_render();
	}
}

const sf::Texture *
Application::_getTextureForBoardItem(yas::BoardItem item) const
{
	using yas::BoardItem;

	switch (item) {
	case BoardItem::Box:
		return &mTextureBox;
	case BoardItem::BoxOnGoal:
		return &mTextureBoxOnGoal;
	case BoardItem::Goal:
		return &mTextureGoal;
	case BoardItem::Floor:
		return &mTextureFloor;
	case BoardItem::Player:
	case BoardItem::PlayerOnGoal:
		return _getTextureForPlayer();
	case BoardItem::Wall:
		return &mTextureWall;
	}

	return nullptr;
}

const sf::Texture *
Application::_getTextureForPlayer() const
{
	using yas::Direction;

	if (!mLastPlayerDirection)
		return &mTexturePlayerDown;

	switch (*mLastPlayerDirection) {
	case Direction::Down:
		return &mTexturePlayerDown;
	case Direction::Left:
		return &mTexturePlayerLeft;
	case Direction::Right:
		return &mTexturePlayerRight;
	case Direction::Up:
		return &mTexturePlayerUp;
	}
}

bool
Application::_loadAssets(std::string const& assetDir)
{
	const std::vector<std::pair<sf::Texture*, std::string>> textures {
		{ &mTextureBox,		"box.png",		},
		{ &mTextureBoxOnGoal,	"box_on_goal.png",	},
		{ &mTextureGoal,	"goal.png",		},
		{ &mTextureFloor,	"ground.png",		},
		{ &mTexturePlayerDown,	"player_down.png",	},
		{ &mTexturePlayerLeft,	"player_left.png",	},
		{ &mTexturePlayerRight,	"player_right.png",	},
		{ &mTexturePlayerUp,	"player_up.png",	},
		{ &mTextureWall,	"wall.png",		},
	};

	// Load textures.
	for (auto const& entry : textures) {
		const auto path = assetDir + "/textures/" + entry.second;
		if (!entry.first->loadFromFile(path)) {
			printf("error, could not load texture: %s\n",
				path.c_str());
			return false;
		}
	}

	// Load board library.
	const std::string libraryPath = assetDir + "/levels.db";
	mBoardLibrary = yas::BoardLibrary::CreateFromFile(libraryPath);
	if (!mBoardLibrary) {
		printf("error, could not load board library: %s\n",
			libraryPath.c_str());
		return false;
	}

	printf("Found %zu boards in the library\n", mBoardLibrary->count());

	return true;
}

void
Application::_processEvent(sf::Event const& event)
{
	using yas::Direction;

	if (event.type == sf::Event::Closed) {
		mIsRunning = false;
	} else if (event.type == sf::Event::KeyPressed) {
		auto prevPlayerPos = mCurrBoard->getPlayerPosition();
		std::optional<Direction> dir;
		bool didUndo = false;

		switch (event.key.code) {
		case sf::Keyboard::Escape:
			mIsRunning = false;
			break;
		case sf::Keyboard::Down:
			dir = Direction::Down;
			break;
		case sf::Keyboard::Left:
			dir = Direction::Left;
			break;
		case sf::Keyboard::Right:
			dir = Direction::Right;
			break;
		case sf::Keyboard::Up:
			dir = Direction::Up;
			break;
		case sf::Keyboard::Space: {
			didUndo = mCurrBoard->undoLastPlayerMove();
			break;
		}
		case sf::Keyboard::N:
			// Load the next level.
			mCurrBoardInd =
				(mCurrBoardInd + 1) % mBoardLibrary->count();
			mCurrBoard = mBoardLibrary->get(mCurrBoardInd);
			break;
		default:
			break;
		}

		if (dir) {
			mCurrBoard->movePlayer(*dir);
			mLastPlayerDirection = *dir;
		} else if (didUndo) {
			// If the player position moved becaused of an
			// undo operation, update its direction.	
			auto currPlayerPos = mCurrBoard->getPlayerPosition();
			if (prevPlayerPos.x < currPlayerPos.x)
				mLastPlayerDirection = Direction::Left;
			else if (prevPlayerPos.x > currPlayerPos.x)
				mLastPlayerDirection = Direction::Right;
			else if (prevPlayerPos.y < currPlayerPos.y)
				mLastPlayerDirection = Direction::Up;
			else if (prevPlayerPos.y > currPlayerPos.y)
				mLastPlayerDirection = Direction::Down;
		}
	}
}

void
Application::_render()
{
	mWindow.clear(sf::Color::Black);

	sf::Sprite sprite;
	sprite.setScale(1.f / kTileSpriteS, 1.f / kTileSpriteS);

	// TODO: Determine center position for the board.
	const ssize_t boardBegX = 0;
	const ssize_t boardBegY = 0;

	// Determine board size.
	auto boardSize = mCurrBoard->getSize();

	// Draw each board items.
	for (size_t y = 0; y < kMaxBoardH; ++y) {
		for (size_t x = 0; x < kMaxBoardW; ++x) {
			auto spritePosX = kTileSpriteW * x;
			auto spritePosY = kTileSpriteW * y;

			bool insideBoard = (x >= boardBegX)
				&& (x - boardBegX) < boardSize.w
				&& (y >= boardBegY)
				&& (y - boardBegY) < boardSize.h;
			if (!insideBoard)
				continue;

			// Always render the floor first.
			sprite.setPosition(sf::Vector2f(
				(float)spritePosX, (float)spritePosY));
			sprite.setTexture(mTextureFloor);
			mWindow.draw(sprite);

			// Look-up the appropriate texture based on the
			// underlying board item. Default to rendering
			// a wall if no items are found.
			const sf::Texture *texture = &mTextureWall;
			auto item = mCurrBoard->getItemAt({x, y});
			if (item) {
				texture = _getTextureForBoardItem(*item);
			}

			if (texture) {
				sprite.setTexture(*texture);
				mWindow.draw(sprite);
			}
		}
	}

	mWindow.display();
}

