#include "application.hh"

#include <cstdio>
#include <memory>

int
main(int argc, char *argv[])
{
	if (argc != 2) {
		printf("usage: %s [asset_dir]\n", argv[0]);
		return -1;
	}

	auto app = Application::Create(argv[1]);
	if (!app) {
		printf("could not create application\n");
		return -1;
	}

	app->run();

	return 0;
}

