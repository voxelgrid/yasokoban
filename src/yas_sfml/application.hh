#pragma once

#include <yas/board.hh>
#include <yas/board_item.hh>
#include <yas/board_library.hh>
#include <yas/direction.hh>

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <memory>
#include <optional>
#include <string>

class Application {
public:
	/// Creates a non-running application.
	static std::unique_ptr<Application> Create(
		std::string const& assetDir);

	/// Runs the application.
	void run();

private:
	Application();

	const sf::Texture *_getTextureForBoardItem(yas::BoardItem item) const;
	const sf::Texture *_getTextureForPlayer() const;
	bool _loadAssets(std::string const& assetDir);
	void _processEvent(sf::Event const& event);
	void _render();

private:
	bool mIsRunning;

	sf::RenderWindow mWindow;
	sf::Texture mTextureBox;
	sf::Texture mTextureBoxOnGoal;
	sf::Texture mTextureGoal;
	sf::Texture mTextureFloor;
	sf::Texture mTexturePlayerDown;
	sf::Texture mTexturePlayerLeft;
	sf::Texture mTexturePlayerRight;
	sf::Texture mTexturePlayerUp;
	sf::Texture mTextureWall;

	size_t mCurrBoardInd;
	std::unique_ptr<yas::Board> mCurrBoard;
	std::unique_ptr<yas::BoardLibrary> mBoardLibrary;
	std::optional<yas::Direction> mLastPlayerDirection;
};

