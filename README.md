# Yet Another Sokoban (YAS)

## Dependencies

	* SFML 2.5

## Build and run

	# Build
	$ mkdir build
	$ cmake -S . -B build
	$ make -C build

	# Run
	./build/src/yas_sfml/yas_sfml assets/
